#ifndef Arrowhpp
#define Arrowhpp 1

#include "Arrow.hpp"
#include "Vertex.hpp"
#include <string>


class Graph
{
 private:
	int nr_vertics; //number of vertics
	Arrow* adiancy_lists;
 public:
	Graph(const std::string path_init_file);

	void diference(Graph* g);	
};

#endif //Arrowhpp