#ifndef Arrow_hpp
#define Arrow_hpp 1

#include "Vertex.hpp"

struct Arrow
{
	int vertixInfo;
	Vertix* nextVrrtix;
};
#endif //Arrow_hpp